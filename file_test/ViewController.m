//
//  ViewController.m
//  file_test
//
//  Created by apple on 08/10/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

{
    UILabel*label;
    UILabel*personalLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createImage];
    [self createLabel];
    [self createDoneButton];
    [self createPassButton];
   [self createPhotoImage];
    [self createPersonalLabel];
    [self createView];
    [self createView1];
    
    
    UITextField *nameField =[self textField:246 tagNumber:2 placeholder:@"  NISHANT" keyboardType:UIKeyboardTypeDefault];
    [self.view addSubview:nameField];
    
    UITextField *lastNameField =[self textField:296 tagNumber:3 placeholder:@"  MITTAL" keyboardType:UIKeyboardTypeDefault];
    [self.view addSubview:lastNameField];
    
    UITextField *emailField =[self textField:345 tagNumber:4 placeholder:@"  nishant.mittal@EMAIL" keyboardType:UIKeyboardTypeEmailAddress];
    [self.view addSubview:emailField];
    
    UITextField *phoneField =[self textField:395 tagNumber:5 placeholder:@"  123458696" keyboardType:UIKeyboardTypePhonePad];
    [self.view addSubview:phoneField];
}

    

#pragma marks CreateImage-

-(void)createImage{
   UIImage *backgroundImage=[UIImage imageNamed:@"bg_iphone4.png"];
   UIImageView *backGroundImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height)];
   backGroundImageView.image=backgroundImage;
   [self.view insertSubview:backGroundImageView atIndex:0];
    
}

-(void)createPhotoImage{
   UIImageView*backgroundview=[[UIImageView alloc]initWithFrame:CGRectMake(94, 85, 113, 113)];
   [backgroundview setImage:[UIImage imageNamed:@"nik.jpg"]];
    backgroundview.layer.cornerRadius=113/2;
    backgroundview.clipsToBounds=YES;
    [self.view addSubview:backgroundview];
    
}



#pragma marks CreateTextField-

- (UITextField *)textField:(float)yAxis  tagNumber:(int)tag placeholder:name keyboardType:(UIKeyboardType *)keyboardType1 {
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(12, yAxis, 294, 40)];
    textField.backgroundColor=[UIColor clearColor];
    textField.layer.borderWidth=1.0f;
    textField.layer.borderColor=[[UIColor grayColor]CGColor];
    textField.placeholder=name;
    textField.tag=tag;
    textField.keyboardType=keyboardType1;
    
    return textField;
}


#pragma marks CreateLable-

-(void)createLabel{
    label=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width , 70)];
    label.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    [label setText:@"EDIT PROFILE"];
    label.textAlignment=NSTextAlignmentCenter;
    label.textColor=[UIColor whiteColor];
    
    
    [self.view addSubview:label];
    [self createBackButton];
}

-(void)createPersonalLabel{
   personalLabel =[[UILabel alloc]initWithFrame:CGRectMake(10, 200, 150 , 50)];
     personalLabel.textColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    [personalLabel setText:@"Personal Info"];
    [self.view addSubview:personalLabel];
}


#pragma marks CreateButton-


-(void)createDoneButton{
    UIButton*doneButton=[[UIButton alloc]initWithFrame:CGRectMake(12, 470, 294, 45)];
    doneButton.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    [doneButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self.view addSubview:doneButton];
}


-(void)createPassButton{
    UIButton*passButton=[[UIButton alloc]initWithFrame:CGRectMake(12, 520, 294, 45)];
    passButton.backgroundColor=[UIColor clearColor];
    [passButton setTitle:@"CHANGE PASSWORD" forState:UIControlStateNormal];
    [self.view addSubview:passButton];
}



-(void)createBackButton{
    UIButton*backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 20, 50, 50)];
    backButton.backgroundColor= [UIColor clearColor] ;
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self.view addSubview:backButton];
}

#pragma marks CreatView-


-(void)createView{
    UIView*lineView=[[UIView alloc]initWithFrame:CGRectMake(115, 225, 200, 1)];
    lineView.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    [self.view addSubview:lineView];
    
}

-(void)createView1{
    UIView*lineView1=[[UIView alloc]initWithFrame:CGRectMake(115, 227, 200, 1)];
    lineView1.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:0.8] ;
    [self.view addSubview:lineView1];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
